package rs.itcentar.achievements.dal;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
    
    Optional<T> findOne(String id);
    
    List<T> findAll();
    
    Optional<T> save(T t);
    
    Optional<T> delete(String id);
    
    Optional<T> update(String id, T t);
    
    List<T> getAchievements(String gameId);
}
