package rs.itcentar.achievements.api;

import org.glassfish.jersey.server.ResourceConfig;
import rs.itcentar.achievements.api.resources.AchievementsResource;

public class AchievementsApp extends ResourceConfig {

    public AchievementsApp() {
        packages("rs.itcentar.achievements.api.resources");
        register(AchievementsResource.class);
    }
    
}
