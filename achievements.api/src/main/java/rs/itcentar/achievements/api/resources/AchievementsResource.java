package rs.itcentar.achievements.api.resources;

import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.List;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.annotation.JacksonFeatures;
import rs.itcentar.achievements.dal.AchievementsRepo;
import rs.itcentar.achievements.data.Achievement;

// BASE URL -> http://localhost:8080/achievements.api/api/

// http://localhost:8080/achievements.api/api/achievements
@Path("achievements")
public class AchievementsResource {
    
    // http://localhost:8080/achievements.api/api/achievements/
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createAchievement(Achievement achievement) {
        AchievementsRepo repo = new AchievementsRepo();
        Optional<Achievement> ret = repo.save(achievement);
        if (ret.isPresent()) {
            return Response.ok(ret.get()).build();
        }
        
        return Response.notModified().build();
    }
    
    // http://localhost:8080/achievements.api/api/achievements/all/{gameId}
    @Path("/all/{gameId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @JacksonFeatures(serializationEnable = SerializationFeature.INDENT_OUTPUT,
            serializationDisable = SerializationFeature.FAIL_ON_EMPTY_BEANS)
    public Response getAllGameAchievements(@NotNull @PathParam("gameId") String gameId) {
        AchievementsRepo repo = new AchievementsRepo();
        List<Achievement> achievements = repo.getAchievements(gameId);
        return Response.ok(achievements).build();
    }
    
    // http://localhost:8080/achievements.api/api/achievements/{achId}
    @Path("/{achId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = SerializationFeature.INDENT_OUTPUT)
    public Response getAchievement(@NotNull @PathParam("achId") String achId) {
        AchievementsRepo repo = new AchievementsRepo();
        Optional<Achievement> ret = repo.findOne(achId);
        if (ret.isPresent()) {
            return Response.ok(ret.get()).build();
        }
        
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    
    // http://localhost:8080/achievements.api/api/achievements/{archId}
    @Path("/{achId}")
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateAchievement(@NotNull @PathParam("achId") String achId,
            Achievement achievement) {
        AchievementsRepo repo = new AchievementsRepo();
        Optional<Achievement> ret = repo.update(achId, achievement);
        if (ret.isPresent()) {
            return Response.ok(ret.get()).build();
        }
        
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    
    
    // http://localhost:8080/achievements.api/api/achievements/{archId}
    @Path("/{achId}")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteAchievement(@NotNull @PathParam("achId") String achId) {
        AchievementsRepo repo = new AchievementsRepo();
        Optional<Achievement> ret = repo.delete(achId);
        if (ret.isPresent()) {
            return Response.ok(ret.get()).build();
        }
        
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
