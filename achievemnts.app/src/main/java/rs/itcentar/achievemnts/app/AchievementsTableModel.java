package rs.itcentar.achievemnts.app;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import rs.itcentar.achievements.data.Achievement;

public class AchievementsTableModel extends DefaultTableModel {

    private List<Achievement> list = new ArrayList<>();

    @Override
    public Object getValueAt(int row, int column) {
        Achievement a = list.get(row);
        switch (column) {
            case 0:
                return a.getDisplayName();
            case 1:
                return a.getDescription();
            default:
                throw new IllegalArgumentException("Unknown column!");
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Display Name";
            case 1:
                return "Description";
            default:
                throw new IllegalArgumentException("Unknown column!");
        }
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getRowCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Class<?> getColumnClass(int i) {
        return String.class;
    }

    public void fillList(List<Achievement> list) {
        this.list = list;
        fireTableDataChanged();
    }
}
