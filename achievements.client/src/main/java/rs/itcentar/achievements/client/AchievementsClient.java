package rs.itcentar.achievements.client;

import java.util.Collections;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import rs.itcentar.achievements.data.Achievement;

public class AchievementsClient {
    
    private static final String BASE_URL = "http://localhost:8080/achievements.api/api/";
    private static final Client client = ClientBuilder.newClient();
    
    public Achievement createAchievement(Achievement achievement) {
        WebTarget webTarget = client.target(BASE_URL).path("achievements");
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        
        try (Response response = invocationBuilder.post(Entity.entity(achievement, MediaType.APPLICATION_JSON_TYPE))) {
            if (response.getStatus() == 200) {
                return response.readEntity(Achievement.class);
            }
        }
        
        return null;
    }
    
    public List<Achievement> getAllGameAchievements(String gameId) {
        WebTarget webTarget = client.target(BASE_URL).path("achievements").path("all").path(gameId);
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        try (Response response = invocationBuilder.get()) {
            if (Response.Status.fromStatusCode(response.getStatus()) == Response.Status.OK) {
                return response.readEntity(new GenericType<List<Achievement>>() {});
            }
        }
        
        return Collections.EMPTY_LIST;
    }
    
    public Achievement getAchievement(String achId) {
        WebTarget webTarget = client.target(BASE_URL).path("achievements").path(achId);
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        try (Response response = invocationBuilder.get()) {
            if (response.getStatus() == 200) {
                return response.readEntity(Achievement.class);
            }
        }
        
        return null;
    }
    
    public Achievement updateAchievement(String achId, Achievement achievement) {
        WebTarget webTarget = client.target(BASE_URL).path("achievements").path(achId);
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        try (Response response = invocationBuilder.put(Entity.entity(achievement, MediaType.APPLICATION_JSON_TYPE))) {
            if (response.getStatus() == 200) {
                return response.readEntity(Achievement.class);
            }
        }
        
        return null;
    }
    
    public Achievement deleteAchievement(String achId) {
        WebTarget webTarget = client.target(BASE_URL).path("achievements").path(achId);
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        try (Response response = invocationBuilder.delete()) {
            if (response.getStatus() == 200) {
                return response.readEntity(Achievement.class);
            }
        }
        
        return null;
    }
}
